# brandfolderassets

INTRODUCTION
------------

Brandfolder Assets module provides the ability to access media such as videos, images, PDFs and the like from the BrandFolder API.
You can add the field to the content type to do that to the brandfolder media.

The setting of the popup can be changed from the brandfolder asset configuration
This setting configuration allows the title and extension of the popup to be shown and hidden
And you can also change the label of the field button in the content node.


Brandfolder Asset Setting Configuration:-
  /admin/config/media/brandfolderassets

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/brandfolderassets

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/brandfolderassets


INSTALLATION
--------------------------

To install Introspect module, follow the steps mentioned below.

 - Module file can be downloaded from the link
   https://www.drupal.org/project/brandfolderassets
 - Extract the downloaded file to the modules directory.
 - Goto Extend, find Brandfolder Assets module and choose 'Install'.
 - You have now enabled your module.

