<?php

namespace Drupal\brandfolderassets\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'brand_file_url_plain' formatter.
 *
 * @FieldFormatter(
 *   id = "brand_file_url_plain",
 *   label = @Translation("URL to brandfolder assets file"),
 *   field_types = {
 *     "brandfolderassets"
 *   }
 * )
 */
class BrandUrlPlainFormatter extends DescriptionFileFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $file) {
      assert($file instanceof FileInterface);
      $elements[$delta] = [
        '#markup' => $file->createFileUrl(),
        '#cache' => [
          'tags' => $file->getCacheTags(),
        ],
      ];
    }

    return $elements;   
    
  }

}
