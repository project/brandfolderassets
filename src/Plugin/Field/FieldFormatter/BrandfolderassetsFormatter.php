<?php

/**
 * @file
 * Contains \Drupal\brandfolderassets\Plugin\Field\FieldWidget\BrandfolderassetsFormatter.
 */

namespace Drupal\brandfolderassets\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'brandfolderassets' formatter.
 *
 * @FieldFormatter(
 *   id = "brandfolderassets_formatter",
 *   label = @Translation("formatter"),
 *   field_types = {
 *     "brandfolderassets"
 *   }
 * )
 */
class BrandfolderassetsFormatter extends DescriptionFileFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $file) {
      $item = $file->_referringItem;

      if(isset($file) && in_array($file->getMimeType(), ['image/jpeg','image/png'])){
        if($item->assetsextension == 'mp4'){          
          $elements[$delta] = [
            '#theme' => 'brand_file_link',
            '#file' => $file,
            '#assetsextension' => $item->assetsextension,
            '#assetsurl' => $item->assetsurl,
            '#description' => $this->getSetting('use_description_as_link_text') ? $item->description : NULL,
            '#cache' => [
              'tags' => $file->getCacheTags(),
            ],
          ];
        }else if($item->assetsextension == 'pdf'){ 
          $elements[$delta] = [
            '#markup' => '<a href="'.$item->assetsurl.'"><img data-des="'.$item->assetsurl.'" class="brand-image" src="'.$file->createFileUrl() .'" ></a>',         
          ];
        }else{
          $elements[$delta] = [
            '#markup' => '<img data-des="'.$item->assetsurl.'" class="brand-image" src="'.$file->createFileUrl() .'" >',         
          ];
        }
      }else{
        $elements[$delta] = [
          '#theme' => 'file_link',
          '#file' => $file,
          '#description' => $this->getSetting('use_description_as_link_text') ? $item->description : NULL,
          '#cache' => [
            'tags' => $file->getCacheTags(),
          ],
        ];
      }
      
      // Pass field item attributes to the theme function.
      if (isset($item->_attributes)) {
        $elements[$delta] += ['#attributes' => []];
        $elements[$delta]['#attributes'] += $item->_attributes;
        // Unset field item attributes since they have been included in the
        // formatter output and should not be rendered in the field template.
        unset($item->_attributes);
      }
    }

    return $elements;
  }
}