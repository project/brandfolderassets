<?php

/**
 * @file
 * CustomModalController class.
 */

namespace Drupal\brandfolderassets\Controller;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\AlertCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\image\Entity\ImageStyle;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\file\Entity\File;

class BrandFolderAssetsController extends ControllerBase {

  // load images and information in popup from the brandfolder library
  public function AssetsLibrary(Request $request) {
    
    $field_name       = $_GET['field_name'];
    $field_name_delta       = $_GET['field_name_delta'];
    $fid_field = $field_name.[$field_name_delta]['fids'];
    $assetsfield_name = str_replace('_', '-', $field_name).'-'.$field_name_delta;
    $options = [
      'dialogClass' => 'brandfolderassets-popup-dialog-class',
      'fieldname' => $field_name.'-'.$field_name_delta,
      'width' => '50%',
    ];
    $config = $this->config('brandfolder.settings');
    $api_key = $config->get('api_key');
    $default_brandfolder = $config->get('default_brandfolder');
    $default_collection = $config->get('default_collection');
    $brandfolders_list = $collections_list = [];

    if ($api_key) {
      $bf = brandfolder_api($api_key);
      $messenger = $this->messenger();
      try {
        $brandfolders_list = $bf->getBrandfolders();
      }
      catch (\Exception $e) {
        $messenger->addMessage($this->t('After you enter an API key, you can select a default Brandfolder'));
      }
      try {
        $collections_list = $default_brandfolder ? $bf->getCollectionsInBrandfolder($default_brandfolder) : [];
      }
      catch (\Exception $e) {
        $messenger->addMessage($this->t('After you choose a default Brandfolder, you can select a default collection if you wish'));
      }
    }

    $brandfolders_list['none'] = $this->t('< None >');
    $collections_list['none'] = $this->t('< None >');

    $popuphtml = '';    

    if ($default_brandfolder && isset($bf)) {
      $bf->default_brandfolder_id = $default_brandfolder;
      $perpage = 60;
      if ($default_collection) {
        $assets = $bf->listAssets(['per'=>$perpage,'fields'=>'cdn_url','include'=>'attachments'], $default_collection);
      }
      else {
        $assets = $bf->listAssets(['per'=>$perpage,'fields'=>'cdn_url','include'=>'attachments']);
      }
      if ($assets) {
        $header = '<div class="search-container text-align-center" style="background: #ebebeb;padding: 2%;"><input class="search-field" placeholder="Search..." type="text"><button fild_name="'.$field_name.'" fild_name_delta="'.$field_name_delta.'" type="button" class="button button--primary search-btn">Search</button></div>';
      	$popuphtml .= '<div class="header-container">'.$header.'</div> <div class="container clearfix brandfolderassets-container" id="'.$field_name.'-'.$field_name_delta.'">';
        foreach ($assets->data as $akey => $assetsValue) {
          $relationAid = $assetsValue->relationships->attachments->data[0]->id;
        
          $extension = $assetsValue->attachments[$relationAid]->extension;
          $attachments_url = $assetsValue->attachments[$relationAid]->url;
          $thumbnail_url = $assetsValue->attributes->thumbnail_url;
          $cdn_url = $assetsValue->attributes->cdn_url;
          $name = $assetsValue->attributes->name;
          $config = \Drupal::config('brandfolderassets.settings');
          $config_item_title = $config->get('item_title');
          $config_item_extinction = $config->get('item_extinction');

          $assetsname = strlen($name) > 13 ? substr($name,0,13)."...":$name;
      		$popuphtml .=	'<div class="brand-grid-25 item-float-left text-align-center brand-padding brand-image-item">';
      		$popuphtml .=		'<img data-name="'.$name.'" data-extension="'.$extension.'" class="brand-image-box" data-attributes-cdn-url="'.$cdn_url.'" data-attachments-url="'.$attachments_url.'" src="' . $thumbnail_url . '">';
      		$popuphtml .=		'<div>';
          if(!empty($config_item_title)){
            $popuphtml .=     '<span class="asset-name">'.$assetsname.'</span>';
          }
          if($config_item_extinction){
            $popuphtml .=     '<span class="asset-extension">'.$extension.'</span>';
          }
          $popuphtml .=   '</div>';
          $popuphtml .=   '<div class="brand-image-checkbox-inner">';
      		$popuphtml .=			'<input data-drupal-selector="edit-brandimage-library-select-form-1" type="checkbox" id="edit-brandimage-library-select-form-1--'.$akey.'" name="brandimage_library_select_form['.$akey.']" value="" class="form-checkbox brand-image-checkbox">';
      		$popuphtml .=		'</div>';
      		$popuphtml .=	'</div>';
      	}

      	$popuphtml .= '</div>';
        $prevBtnDisable = $nextBtnDisable = '';
        if(!$assets->meta->prev_page){
          $prevBtnDisable = 'disablebtn';
        }
        $nextBtnDisable = '';
        if(!$assets->meta->next_page){
          $nextBtnDisable = 'disablebtn';
        }

        $popuphtml .= '<div class="pagination"><button fild_name="'.$field_name.'" fild_name_delta="'.$field_name_delta.'" currentpage="'.$assets->meta->current_page.'" nextpage="'.$assets->meta->next_page.'" prevpage="'.$assets->meta->prev_page.'" totalpages="'.$assets->meta->total_pages.'" type="button" class="'.$prevBtnDisable.' button button--primary js-form-submit form-submit ui-button ui-corner-all ui-widget brand-asset-back-btn">Previous</button><span style="padding-left: 1%;">'.$assets->meta->current_page.'/'.$assets->meta->total_pages.'</span><button fild_name="'.$field_name.'" fild_name_delta="'.$field_name_delta.'" currentpage="'.$assets->meta->current_page.'" nextpage="'.$assets->meta->next_page.'" prevpage="'.$assets->meta->prev_page.'" totalpages="'.$assets->meta->total_pages.'" type="button" class="'.$nextBtnDisable.' button button--primary js-form-submit form-submit ui-button ui-corner-all ui-widget brand-asset-next-btn">Next</button></div>';

      	$popuphtml .= '<button fid_field="'.$fid_field.'" fild_name="'.$field_name.'" fild_name_delta="'.$field_name_delta.'" assetsfield_name="'.$assetsfield_name.'" type="button" class="button button--primary js-form-submit form-submit ui-button ui-corner-all ui-widget brand-image-insert-btn">Insert Assets</button>';     

         $popuphtml .= '<div class="overlay hidden">
                          <div class="overlay__inner">
                              <div class="overlay__content"><span class="spinner"></span></div>
                          </div>
                      </div>';
           
      }
    }

    $response = new AjaxResponse();
    $response->addCommand(new OpenModalDialogCommand(t('Brandfolder Assets'),$popuphtml, $options));

    return $response;
  }

  //save images in system from the brandfolder library
	public function AssetsSave(Request $request) {
    $output = [];
		$dataextension       = $request->request->get('data_extension');
    $dataattachmentsurl       = $request->request->get('data_attachments_url');
    $remoteFilePath       = $request->request->get('data_attributes_cdnurl');
    $data_thumbnail_url       = $request->request->get('data_thumbnail_url');
    $data_name       = $request->request->get('data_name');
    if($remoteFilePath){
      $directory = \Drupal::service('stream_wrapper_manager')->normalizeUri(\Drupal::config('system.file')->get('default_scheme') . ('://' . 'brandfolderassets'));
      if (!\Drupal::service('file_system')->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY)) {
        // If our directory doesn't exist and can't be created, use the default.
        $directory = NULL;
      }
      if($dataextension  == 'mp4'){
        $file = system_retrieve_file($data_thumbnail_url, $directory.'/brandvideo.jpg', TRUE);
      }else{
        $file = system_retrieve_file($remoteFilePath, $directory, TRUE);
      }
     
  		$output = ['status' => 200,'dataextension'=>$dataextension, 'dataattachmentsurl'=>$dataattachmentsurl,  'fid'=>$file->id(), 'imgurl'=>$file->createFileUrl() , 'msg' => 'Saved successfully.'];
    }
    return new JsonResponse($output);
	}  

  // load images and information with Pagination in popup from the brandfolder library
  public function AssetsPagination(Request $request,$totalpage,$viewpage) {
    $field_name       = $request->request->get('field_name');
    $field_name_delta       = $request->request->get('field_name_delta');
    $fid_field = $field_name.[$field_name_delta]['fids'];
    $assetsfield_name = str_replace('_', '-', $field_name).'-'.$field_name_delta;
    
    $config = $this->config('brandfolder.settings');
    $api_key = $config->get('api_key');
    $default_brandfolder = $config->get('default_brandfolder');
    $default_collection = $config->get('default_collection');
    $brandfolders_list = $collections_list = [];

    if ($api_key) {
      $bf = brandfolder_api($api_key);
      $messenger = $this->messenger();
      try {
        $brandfolders_list = $bf->getBrandfolders();
      }
      catch (\Exception $e) {
        $messenger->addMessage($this->t('After you enter an API key, you can select a default Brandfolder'));
      }
      try {
        $collections_list = $default_brandfolder ? $bf->getCollectionsInBrandfolder($default_brandfolder) : [];
      }
      catch (\Exception $e) {
        $messenger->addMessage($this->t('After you choose a default Brandfolder, you can select a default collection if you wish'));
      }
    }

    $brandfolders_list['none'] = $this->t('< None >');
    $collections_list['none'] = $this->t('< None >');

    $popuphtml = '';    

    if ($default_brandfolder && isset($bf)) {
      $bf->default_brandfolder_id = $default_brandfolder;
      $perpage = 60;
      if ($default_collection) {
        $assets = $bf->listAssets(['page'=>$viewpage,'per'=>$perpage,'fields'=>'cdn_url','include'=>'attachments'], $default_collection);
      }
      else {
        $assets = $bf->listAssets(['page'=>$viewpage,'per'=>$perpage,'fields'=>'cdn_url','include'=>'attachments']);
      }
      if ($assets) {
        // print "<pre>" ; print_r($assets);die;
        $header = '<div class="search-container text-align-center" style="background: #6b6b6b;padding: 2%;"><input class="search-field" placeholder="Search..." type="text"><button fild_name="'.$field_name.'" fild_name_delta="'.$field_name_delta.'" type="button" class="button button--primary search-btn">Search</button></div>';
        $popuphtml .= '<div class="header-container">'.$header.'</div> <div class="container clearfix brandfolderassets-container" id="'.$field_name.'-'.$field_name_delta.'">';
        foreach ($assets->data as $akey => $assetsValue) {
          $relationAid = $assetsValue->relationships->attachments->data[0]->id;
        
          $extension = $assetsValue->attachments[$relationAid]->extension;
          $attachments_url = $assetsValue->attachments[$relationAid]->url;
          $thumbnail_url = $assetsValue->attributes->thumbnail_url;
          $cdn_url = $assetsValue->attributes->cdn_url;
          $name = $assetsValue->attributes->name;

          $assetsname = strlen($name) > 13 ? substr($name,0,13)."...":$name;
          $popuphtml .= '<div class="brand-grid-25 item-float-left text-align-center brand-padding brand-image-item">';
          $popuphtml .=   '<img data-name="'.$name.'" data-extension="'.$extension.'" class="brand-image-box" data-attributes-cdn-url="'.$cdn_url.'" data-attachments-url="'.$attachments_url.'" src="' . $thumbnail_url . '">';
          $popuphtml .=   '<div>';
          $popuphtml .=     '<span class="asset-name">'.$assetsname.'</span>';
          $popuphtml .=     '<span class="asset-extension">'.$extension.'</span>';
          $popuphtml .=   '</div>';
          $popuphtml .=   '<div class="brand-image-checkbox-inner">';
          $popuphtml .=     '<input data-drupal-selector="edit-brandimage-library-select-form-1" type="checkbox" id="edit-brandimage-library-select-form-1--'.$akey.'" name="brandimage_library_select_form['.$akey.']" value="" class="form-checkbox brand-image-checkbox">';
          $popuphtml .=   '</div>';
          $popuphtml .= '</div>';
        }

        $popuphtml .= '</div>';

        $prevBtnDisable = $nextBtnDisable = '';
        if(!$assets->meta->prev_page){
          $prevBtnDisable = 'disablebtn';
        }
        $nextBtnDisable = '';
        if(!$assets->meta->next_page){
          $nextBtnDisable = 'disablebtn';
        }

        $popuphtml .= '<div class="pagination"><button fild_name="'.$field_name.'" fild_name_delta="'.$field_name_delta.'" currentpage="'.$assets->meta->current_page.'" nextpage="'.$assets->meta->next_page.'" prevpage="'.$assets->meta->prev_page.'" totalpages="'.$assets->meta->total_pages.'" type="button" class="'.$prevBtnDisable.' button button--primary js-form-submit form-submit ui-button ui-corner-all ui-widget brand-asset-back-btn">Previous</button><span style="padding-left: 1%;">'.$assets->meta->current_page.'/'.$assets->meta->total_pages.'</span><button fild_name="'.$field_name.'" fild_name_delta="'.$field_name_delta.'" currentpage="'.$assets->meta->current_page.'" nextpage="'.$assets->meta->next_page.'" prevpage="'.$assets->meta->prev_page.'" totalpages="'.$assets->meta->total_pages.'" type="button" class="'.$nextBtnDisable.' button button--primary js-form-submit form-submit ui-button ui-corner-all ui-widget brand-asset-next-btn">Next</button></div>';

        $popuphtml .= '<button fid_field="'.$fid_field.'" fild_name="'.$field_name.'" fild_name_delta="'.$field_name_delta.'" assetsfield_name="'.$assetsfield_name.'" type="button" class="button button--primary js-form-submit form-submit ui-button ui-corner-all ui-widget brand-image-insert-btn">Insert Assets</button>';     

         $popuphtml .= '<div class="overlay hidden">
                          <div class="overlay__inner">
                            <div class="overlay__content"><span class="spinner"></span></div>
                          </div>
                        </div>';
           
      }
    }
    
    $response = new AjaxResponse();
    $response->addCommand(new HtmlCommand('.brandfolderassets-popup-dialog-class', $popuphtml));

    return $response;
  }

  // load images and information with search in popup from the brandfolder library
  public function AssetsSearch(Request $request,$searchtype,$searchvalue) {

    $search = '';
    $searchname = '';
    if(!empty($searchvalue) && $searchvalue != 'nosearch'){
      $search = $searchtype.':'.$searchvalue;
      $searchname = $searchvalue;
    }

    $field_name       = $request->request->get('field_name');
    $field_name_delta       = $request->request->get('field_name_delta');
    $fid_field = $field_name.[$field_name_delta]['fids'];
    $assetsfield_name = str_replace('_', '-', $field_name).'-'.$field_name_delta;
    
    $config = $this->config('brandfolder.settings');
    $api_key = $config->get('api_key');
    $default_brandfolder = $config->get('default_brandfolder');
    $default_collection = $config->get('default_collection');
    $brandfolders_list = $collections_list = [];

    if ($api_key) {
      $bf = brandfolder_api($api_key);
      $messenger = $this->messenger();
      try {
        $brandfolders_list = $bf->getBrandfolders();
      }
      catch (\Exception $e) {
        $messenger->addMessage($this->t('After you enter an API key, you can select a default Brandfolder'));
      }
      try {
        $collections_list = $default_brandfolder ? $bf->getCollectionsInBrandfolder($default_brandfolder) : [];
      }
      catch (\Exception $e) {
        $messenger->addMessage($this->t('After you choose a default Brandfolder, you can select a default collection if you wish'));
      }
    }

    $brandfolders_list['none'] = $this->t('< None >');
    $collections_list['none'] = $this->t('< None >');

    $popuphtml = '';    

    if ($default_brandfolder && isset($bf)) {
      $bf->default_brandfolder_id = $default_brandfolder;
      $perpage = 60;
      if ($default_collection) {
        $assets = $bf->listAssets(['search'=>$search,'per'=>$perpage,'fields'=>'cdn_url','include'=>'attachments'], $default_collection);
      }
      else {
        $assets = $bf->listAssets(['search'=>$search,'per'=>$perpage,'fields'=>'cdn_url','include'=>'attachments']);
      }
      if ($assets) {
        // print "<pre>" ; print_r($assets);die;
        $header = '<div class="search-container text-align-center" style="background: #6b6b6b;padding: 2%;"><input class="search-field" value="'.$searchname.'" placeholder="Search..." type="text"><button type="button" class="button button--primary search-btn">Search</button></div>';
        $popuphtml .= '<div class="header-container">'.$header.'</div> <div class="container clearfix brandfolderassets-container" id="'.$field_name.'-'.$field_name_delta.'">';
        foreach ($assets->data as $akey => $assetsValue) {
          $relationAid = $assetsValue->relationships->attachments->data[0]->id;
        
          $extension = $assetsValue->attachments[$relationAid]->extension;
          $attachments_url = $assetsValue->attachments[$relationAid]->url;
          $thumbnail_url = $assetsValue->attributes->thumbnail_url;
          $cdn_url = $assetsValue->attributes->cdn_url;
          $name = $assetsValue->attributes->name;

          $assetsname = strlen($name) > 13 ? substr($name,0,13)."...":$name;
          $popuphtml .= '<div class="brand-grid-25 item-float-left text-align-center brand-padding brand-image-item">';
          $popuphtml .=   '<img data-name="'.$name.'" data-extension="'.$extension.'" class="brand-image-box" data-attributes-cdn-url="'.$cdn_url.'" data-attachments-url="'.$attachments_url.'" src="' . $thumbnail_url . '">';
          $popuphtml .=   '<div>';
          $popuphtml .=     '<span class="asset-name">'.$assetsname.'</span>';
          $popuphtml .=     '<span class="asset-extension">'.$extension.'</span>';
          $popuphtml .=   '</div>';
          $popuphtml .=   '<div class="brand-image-checkbox-inner">';
          $popuphtml .=     '<input data-drupal-selector="edit-brandimage-library-select-form-1" type="checkbox" id="edit-brandimage-library-select-form-1--'.$akey.'" name="brandimage_library_select_form['.$akey.']" value="" class="form-checkbox brand-image-checkbox">';
          $popuphtml .=   '</div>';
          $popuphtml .= '</div>';
        }

        $popuphtml .= '</div>';

        $prevBtnDisable = $nextBtnDisable = '';
        if(!$assets->meta->prev_page){
          $prevBtnDisable = 'disablebtn';
        }
        $nextBtnDisable = '';
        if(!$assets->meta->next_page){
          $nextBtnDisable = 'disablebtn';
        }

        $popuphtml .= '<div class="pagination"><button fild_name="'.$field_name.'" fild_name_delta="'.$field_name_delta.'" currentpage="'.$assets->meta->current_page.'" nextpage="'.$assets->meta->next_page.'" prevpage="'.$assets->meta->prev_page.'" totalpages="'.$assets->meta->total_pages.'" type="button" class="'.$prevBtnDisable.' button button--primary js-form-submit form-submit ui-button ui-corner-all ui-widget brand-asset-back-btn">Previous</button><span style="padding-left: 1%;">'.$assets->meta->current_page.'/'.$assets->meta->total_pages.'</span><button fild_name="'.$field_name.'" fild_name_delta="'.$field_name_delta.'" currentpage="'.$assets->meta->current_page.'" nextpage="'.$assets->meta->next_page.'" prevpage="'.$assets->meta->prev_page.'" totalpages="'.$assets->meta->total_pages.'" type="button" class="'.$nextBtnDisable.' button button--primary js-form-submit form-submit ui-button ui-corner-all ui-widget brand-asset-next-btn">Next</button></div>';

        $popuphtml .= '<button fid_field="'.$fid_field.'" fild_name="'.$field_name.'" fild_name_delta="'.$field_name_delta.'" assetsfield_name="'.$assetsfield_name.'" type="button" class="button button--primary js-form-submit form-submit ui-button ui-corner-all ui-widget brand-image-insert-btn">Insert Assets</button>';     

         $popuphtml .= '<div class="overlay hidden">
                          <div class="overlay__inner">
                              <div class="overlay__content"><span class="spinner"></span></div>
                          </div>
                      </div>';
           
      }
    }
    
    $response = new AjaxResponse();
    $response->addCommand(new HtmlCommand('.brandfolderassets-popup-dialog-class', $popuphtml));

    return $response;
  }

}

