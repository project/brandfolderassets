<?php

namespace Drupal\brandfolderassets\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Define the administrative form used to configure the Brandfolder integration.
 */
class BrandFolderAssetsSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'brandfolderassets.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'brandfolderassets_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('brandfolderassets.settings');
    $item_title = $config->get('item_title');
    $item_extinction = $config->get('item_extinction');
    $field_item_label = $config->get('field_item_label');

    $form['popup_item_setting'] = [
      '#type'  => 'details',
      '#title' => $this->t('Popup Settings'),
      '#open' => TRUE,
    ];
    
    $form['popup_item_setting']['item_title'] = [
      '#type' => 'checkbox', 
      '#title' => t('Title'),
      '#default_value' => $item_title ? $item_title : '0',
    ];

    $form['popup_item_setting']['item_extinction'] = [
      '#type' => 'checkbox', 
      '#title' => t('Extension'),
      '#default_value' => $item_extinction ? $item_extinction : '0',
    ];

    $form['popup_item_setting']['item_description'] =[  
      '#markup' => t('This setting will work on assets popup in field.'),
    ];    

    $form['field_item_setting'] = [
      '#type'  => 'details',
      '#title' => $this->t('Field Settings'),
      '#open' => TRUE,
    ];
    
    $form['field_item_setting']['field_item_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Field Button Label'),
      '#description' => $this->t('This label will reflect on field button name, that is by default displaying as "Add Brandfolder Assets".'),      
      '#attributes'=>['placeholder'=>'Add Brandfolder Assets'],
      '#default_value' => $field_item_label ? $field_item_label : '',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('brandfolderassets.settings');
    $config->set('item_title', $form_state->getValue('item_title'));
    $config->set('item_extinction', $form_state->getValue('item_extinction'));
    $config->set('field_item_label', $form_state->getValue('field_item_label'));
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
