jQuery(document).ready(function(){  
  //selected image in brandfolder asset popup
  jQuery(document).on('click','.brand-image-checkbox',function () {
    jQuery(this).closest('.brand-image-item').toggleClass('brand-library-item--enabled brand-library-item--disabled');
    jQuery('.brand-image-item').toggleClass('brand-library-item--disabled');

    if (jQuery('.brand-image-item').hasClass("brand-library-item--disabled")) {
      jQuery('.brand-image-item input[type="checkbox"]').prop('disabled', true);
    }else{
      jQuery('.brand-image-item input[type="checkbox"]').prop('disabled', false);
    }

    if (jQuery(this).closest('.brand-image-item').hasClass("brand-library-item--enabled")) {
      jQuery(this).prop('disabled', false);
    }
  });

  //insertd selected image in node of field
  jQuery(document).on('click','.brand-image-insert-btn',function () {
    if (jQuery('.brand-image-item').hasClass("brand-library-item--enabled")) {
      var DataExtension = jQuery('.brand-library-item--enabled .brand-image-box').attr('data-extension');
      var DataAttachmentsUrl = jQuery('.brand-library-item--enabled .brand-image-box').attr('data-attachments-url');
      var DataAttributesCdnUrl = jQuery('.brand-library-item--enabled .brand-image-box').attr('data-attributes-cdn-url');
      var DataThumbnailUrl = jQuery('.brand-library-item--enabled .brand-image-box').attr('src');
      var DataName = jQuery('.brand-library-item--enabled .brand-image-box').attr('data-name');

      jQuery('.ui-icon-closethick').trigger('click');
      
      var module_url = drupalSettings.brandfolderassets.module_url;

      var fieldname = jQuery(this).attr('fild_name');

      var fildnamedelta = jQuery(this).attr('fild_name_delta');

      var field_name_delta = jQuery(this).attr('fild_name')+'['+jQuery(this).attr('fild_name_delta')+']';
         
      var assetsfield_name = jQuery(this).attr('assetsfield_name');

      jQuery.ajax({
        url: drupalSettings.brandfolderassets.site_url+'brandfolderassets/save',
        type: 'POST',
        data: {data_attributes_cdnurl: DataAttributesCdnUrl,data_extension:DataExtension,data_attachments_url:DataAttachmentsUrl, data_thumbnail_url:DataThumbnailUrl, data_name:DataName},
        dataType: 'json',
        beforeSend: function() {
        },
        success: function(response) {
          var ResimgUrl = response.imgurl;
          var Resdataextension = response.dataextension;
          var Resdataattachmentsurl = response.dataattachmentsurl;
          var boxhtml = '<div class="brand-grid-25 text-align-center brand-padding brand-image-item"><img class="brand-image-box" src="'+ResimgUrl+'"><div class="brand-image-checkbox-inner"><span  class="selected-image-remove" data-mode="new" data-fild-name="'+fieldname+'" data-fild-name-delta="'+fildnamedelta+'" data-id="'+assetsfield_name+'"><img src="'+module_url+'/images/remove.svg"></span></div></div>';          

          jQuery('#edit-'+assetsfield_name+' .container-brand-image-widget-box').html(boxhtml);

          jQuery('#edit-'+assetsfield_name+' input[name="'+field_name_delta+'[fids]"]').val(response.fid);
          jQuery('#edit-'+assetsfield_name+' input[name="'+field_name_delta+'[assetsextension]"]').val(Resdataextension);
          jQuery('#edit-'+assetsfield_name+' input[name="'+field_name_delta+'[assetsurl]"]').val(Resdataattachmentsurl);
          
        },
      });

      jQuery('#edit-'+assetsfield_name+' .container-brand-library-image-selection').hide();
    }else{
      console.log('not select');
    }
  });

  //removed selected image in node of field
  jQuery(document).on('click','.selected-image-remove',function () {
    var assetsfield_name = jQuery(this).attr('data-id');
    
    var fidfield = jQuery(this).attr('data-fild-name')+'['+jQuery(this).attr('data-fild-name-delta')+'][fids]';
          
    jQuery('#edit-'+assetsfield_name+' .container-brand-image-widget-box .brand-image-item').remove();

    jQuery('#edit-'+assetsfield_name+' .container-brand-library-image-selection').show();

    if(jQuery(this).attr('data-mode') == 'edit'){
      jQuery('#edit-'+assetsfield_name+' input[name="'+fidfield+'"]').val('');
    } else if(jQuery(this).attr('data-mode') == 'new'){
      jQuery('#edit-'+assetsfield_name+' input[name="'+fidfield+'"]').val('');
    }
  
  });

  //search title in brandfolder asset popup
  jQuery(document).on('click','.search-btn',function () {
    
    var searchvalue = jQuery(this).siblings('.search-field').val();
    console.log('searchvalue '+searchvalue);
    if(!searchvalue){
      searchvalue = 'nosearch';
    }
     
    var searchtype = 'name';
    var field_name = jQuery(this).attr('fild_name');
    var field_name_delta = jQuery(this).attr('fild_name_delta');
    if(searchtype){
      jQuery.ajax({
        url: drupalSettings.brandfolderassets.site_url+'brandfolderassets/search/'+searchtype+'/'+searchvalue,
        type: 'POST',
        data: {field_name: field_name,field_name_delta:field_name_delta},
        dataType: 'json',
        beforeSend: function() {
          jQuery('.brandfolderassets-popup-dialog-class .overlay').removeClass('hidden');
        },
        success: function(response) {
          jQuery('.brandfolderassets-popup-dialog-class .overlay').addClass('hidden');
          jQuery('.brandfolderassets-popup-dialog-class #drupal-modal').html(response[0].data);
        },
      });
    }
  });

  //pagination next
  jQuery(document).on('click','.brand-asset-next-btn',function () {
    var nextpage = jQuery(this).attr('nextpage');
    var totalpages = jQuery(this).attr('totalpages');
    var field_name = jQuery(this).attr('fild_name');
    var field_name_delta = jQuery(this).attr('fild_name_delta');
    if(nextpage){
      jQuery.ajax({
        url: drupalSettings.brandfolderassets.site_url+'brandfolderassets/pagination/'+totalpages+'/'+nextpage,
        type: 'POST',
        data: {field_name: field_name,field_name_delta:field_name_delta},
        dataType: 'json',
        beforeSend: function() {
          jQuery('.brandfolderassets-popup-dialog-class .overlay').removeClass('hidden');
        },
        success: function(response) {
          jQuery('.brandfolderassets-popup-dialog-class .overlay').addClass('hidden');         
          jQuery('.brandfolderassets-popup-dialog-class #drupal-modal').html(response[0].data);
        },
      });
    }
  });

  //pagination back
  jQuery(document).on('click','.brand-asset-back-btn',function () {
    var prevpage = jQuery(this).attr('prevpage');
    var totalpages = jQuery(this).attr('totalpages');
    var field_name = jQuery(this).attr('fild_name');
    var field_name_delta = jQuery(this).attr('fild_name_delta');
    if(prevpage){
      jQuery.ajax({
        url: drupalSettings.brandfolderassets.site_url+'brandfolderassets/pagination/'+totalpages+'/'+prevpage,
        type: 'POST',
        data: {field_name: field_name,field_name_delta:field_name_delta},
        dataType: 'json',
        beforeSend: function() {
          jQuery('.brandfolderassets-popup-dialog-class .overlay').removeClass('hidden');
        },
        success: function(response) {
          jQuery('.brandfolderassets-popup-dialog-class .overlay').addClass('hidden');
          jQuery('.brandfolderassets-popup-dialog-class #drupal-modal').html(response[0].data);
        },
      });
    }

  });

});